<?php

/**
 * @file
 * Home of the FeedsArchiveFetcher and related classes.
 */


/**
 * Result of FeedsArchiveFetcher::fetch().
 */
class FeedsArchiveFetcherResult extends FeedsFetcherResult {

  protected $file_path;

  /**
   * Constructor.
   */
  public function __construct($file_path) {
    parent::__construct('');
    $this->file_path = $file_path;
  }

  /**
   * Overrides parent::getRaw();
   */
  public function getRaw() {
    return $this->sanitizeRaw(file_get_contents($this->file_path));
  }
}

/**
 * Fetches data via HTTP.
 */
class FeedsArchiveFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $source_path = $source_config['source'];
    $source_file = $source_config['source_file'];

    $import_dir = file_directory_temp() . DIRECTORY_SEPARATOR . $source->importer->id;
    if (is_dir($import_dir)) $this->removeDir($import_dir);

    mkdir($import_dir);

    // first copy or download the file into the tmp directory
    $destination = $import_dir . DIRECTORY_SEPARATOR . basename($source_path);
    if (!copy($source_path, $destination)) {
      throw new Exception("Could not copy/download file form $source_path.");
    }

    FeedsArchiveExtractor::extract($destination, $import_dir);

    $target_file = $this->determineTargetFile($source_file, $import_dir);
    if ($target_file === false) {
      throw new Exception("Could not find feeds file in extracted archive in $target_dir");
    }

    $feeds_file = $import_dir . DIRECTORY_SEPARATOR . $target_file;
    return new FeedsArchiveFetcherResult($feeds_file);
  }

  /**
   * Find the feeds import file inside the extracted archive.
   *
   * @param string $source_file
   *   Source file representation. Can be the plain file name or a regex.
   * @param string $archive_dir
   *   The path to the extracted archive.
   *
   * @return
   *  The full path to the file or false if not found.
   */
  protected function determineTargetFile($source_file, $archive_dir) {
    $target_file = false;

    foreach ($this->listFiles($archive_dir) as $file) {
      if (preg_match('|' . $source_file . '|', $file)) {
        $target_file = $file;
        break;
      }
    }

    return $target_file;
  }

  /**
   * Return an array of files in a directory.
   *
   * @param $dir
   *   A stream wreapper URI that is a directory.
   *
   * @return
   *   An array of stream wrapper URIs pointing to files. The array is empty
   *   if no files could be found. Never contains directories.
   */
  protected function listFiles($dir) {
    $dir = file_stream_wrapper_uri_normalize($dir);
    $files = array();
    if ($items = @scandir($dir)) {
      foreach ($items as $item) {
        if (is_file("$dir/$item") && strpos($item, '.') !== 0) {
          $files[] = "$item";
        }
      }
    }
    return $files;
  }

  /**
   * Recursively delete a directory and it's contents.
   *
   * @param string $dir
   *   Path to the directory.
   */
  protected function removeDir($dir) {
    foreach(glob($dir . '/*') as $file) {
        if(is_dir($file))
            $this->removeDir($file);
        else
            unlink($file);
    }
    rmdir($dir);
  }

  /**
   * Clear caches.
   */
  public function clear(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $url = $source_config['source'];
    feeds_include_library('http_request.inc', 'http_request');
    http_request_clear_cache($url);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();

    return $form;
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('Filepath or URL'),
      '#description' => t('Enter an archive file location (either HTTP or local file).'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );

    $form['source_file'] = array(
      '#type' => 'textfield',
      '#title' => t('File in archive'),
      '#description' => t('Enter the filename or a regular expression to determine the file to use inside the archive.'),
      '#default_value' => isset($source_config['source_file']) ? $source_config['source_file'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    if (!feeds_valid_url($values['source'], TRUE)) {
      $form_key = 'feeds][' . get_class($this) . '][source';
      form_set_error($form_key, t('The URL %source is invalid.', array('%source' => $values['source'])));
    }
    if (empty($values['source_file'])) {
       $form_key = 'feeds][' . get_class($this) . '][source_file';
      form_set_error($form_key, t('The path %source_file is invalid.', array('%source_file' => $values['source_file'])));
    }
  }
}

class FeedsArchiveExtractor {
  const TYPE_ZIP = 'zip';
  const TYPE_TAR_GZ = 'tar.gz';
  const TYPE_TAR_BZIP = 'tar.bz';

  public static function extract($archive, $destination) {
    $extractor = new self();
    return $extractor->doExtract($archive, $destination);
  }

  function doExtract($archive, $destination) {
    // try to determine the archive type
    // and check if it is supported
    $type = $this->determineArchiveType($archive);
    if ($type === false) {
      throw new Exception("Could not determine archive type for file $archive.");
    }
    
   $success = $this->extractArchive($type, $archive, $destination);
    if ($success === false) {
      throw new Exception("Could not extract archive $archive of type $type");
    }

    return true;
  }

  protected function determineArchiveType($path) {
    // first try to discover type by extension
    $info = pathinfo($path);
    $extension = false;

    if (isset($info['extension'])) {
      switch (strtolower($info['extension'])) {
        case 'zip':
          $extension = FeedsArchiveExtractor::TYPE_ZIP;
          break;
      }
    }

    return $extension;
  }

  protected function extractArchive($type, $archive, $destination) {
    switch ($type) {

      case self::TYPE_ZIP:
        return $this->extractArchiveZip($archive, $destination);
        break;
      
      default:
        return false;
        break;
    }
  }

  protected function extractArchiveZip($archive, $destination) {
    /* TODO: add PHP ZIP extension support
    if (function_exists('zip_open')) {
  
    }
    */

    // check if unzip program is available (POSIX systems only)
    exec('command -v unzip', $output, $result);
    if ($result === 0) {
      // unzip available, so use it
      exec("unzip -o -d $destination $archive", $output, $code);

      if ($code !== 0) {
        throw new Exception("Could not extract archive $archive using unzip: $code");
      }
    }
    else {
      throw new Exception("No method for extracting ZIP archives available.");
    }

    return true;
  }
}
